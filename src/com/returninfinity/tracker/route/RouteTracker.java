package com.returninfinity.tracker.route;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.returninfinity.locationandroutetracker.R;
import com.returninfinity.tracker.location.LocationTracker;

public class RouteTracker extends MapActivity
{
	private LocationManager locationManager; // gives location data
	private MapView mapView; // displays a Google map
	private MapController mapController; // manages map pan/zoom
	private Location previousLocation; // previous reported location
	private RouteOverlay routeOverlay; // overlay that shows route on map
	private long distanceTraveled; // total distance the user traveled
	private BearingFrameLayout bearingFrameLayout; // rotates the MapView
	private boolean tracking; // whether app is currently tracking
	private long startTime; // time (in milliseconds) when tracking starts
	private PowerManager.WakeLock wakeLock; // used to prevent device sleep
	private boolean gpsFix; // whether we have a GPS fix for accurate data
	private GeoPoint p;
	
	private static final double MILLISECONDS_PER_HOUR = 1000 * 60 * 60;
	private static final double MILES_PER_KILOMETER = 0.621371192;
	private static final int MAP_ZOOM = 18;
	
	@Override
	protected void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.route_main);
		
		// create new MapView using the Google Maps API key
		bearingFrameLayout = new BearingFrameLayout(this, "07yU3C0SR_wD67IOgBKHIJN4-4CzD4uh2CNLMOA");
		
		// add bearingFrameLayout to mainLayout
		FrameLayout mainLayout = (FrameLayout) findViewById(R.id.mainroutelayout);
		mainLayout.addView(bearingFrameLayout, 0);
		
		// get the MapView and MapController
		mapView = bearingFrameLayout.getMapView();
		
		// mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(MAP_ZOOM);
		
		// create map Overlay
		routeOverlay = new RouteOverlay();
		
		// add the RouteOverlay to the MapView list of Overlays
		mapView.getOverlays().add(routeOverlay);
		
		distanceTraveled = 0;
		
		ToggleButton trackingToogleButton = (ToggleButton) findViewById(R.id.trackingToogleButton);
		trackingToogleButton.setOnCheckedChangeListener(trackingToogleButtonListener);
		
		Button locationTracker = (Button) findViewById(R.id.LocationTracking);
		locationTracker.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v)
			{
				Intent intent = new Intent(v.getContext(), LocationTracker.class);
				startActivity(intent);
			}
		});
	}
	
	/**
	 * Called when Activity becomes visible to the user
	 */
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// for specifying the location provider's settings
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setBearingRequired(true);
		criteria.setCostAllowed(true);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		criteria.setAltitudeRequired(false);
		
		// get the LocationManager
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		// register listener to determine whether we have a GPX fix
		locationManager.addGpsStatusListener(gpsStatusListener);
		
		// get the best provider based on our Criteria
		String provider = locationManager.getBestProvider(criteria, true);
		
		locationManager.requestLocationUpdates(provider, 0, 0, locationListener);
		
		// get the app's power manager
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		
		// get a WakeLock preventing the device from sleeping
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "No sleep");
		wakeLock.acquire(); // aquire the wake lock
		
		// redraw the BearingFrameLayout
		bearingFrameLayout.invalidate();
	}
	
	private final LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location)
		{
			p = new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));
			
			// if getting Locations then we have a GPX fix
			gpsFix = true;
			
			// if we're currently tracking
			if (tracking)
			{
				updateLocation(location);
			}
		}
		
		public void onProviderDisabled(String provider)
		{
			
		}
		
		public void onProviderEnabled(String provider)
		{
			
		}
		
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			
		}
	};
	
	private final GpsStatus.Listener gpsStatusListener = new GpsStatus.Listener() {
		
		public void onGpsStatusChanged(int event)
		{
			if (event == GpsStatus.GPS_EVENT_FIRST_FIX)
			{
				gpsFix = true;
				
				Toast results = Toast.makeText(RouteTracker.this, getResources().getString(R.string.toast_signal_acquired), Toast.LENGTH_SHORT);
				
				// center the Toast in the screen
				results.setGravity(Gravity.CENTER, results.getXOffset() / 2, results.getYOffset() / 2);
				results.show();
			}
		}
		
	};
	
	private final OnCheckedChangeListener trackingToogleButtonListener = new OnCheckedChangeListener() {
		
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
			// if app is currently tracking
			if (!isChecked)
			{
				tracking = false;
				
				// compute the total time we were tracking
				long milliseconds = System.currentTimeMillis() - startTime;
				double totalHours = milliseconds / MILLISECONDS_PER_HOUR;
				
				// create a dialog displaying the results
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RouteTracker.this);
				dialogBuilder.setTitle(R.string.results);
				
				double distanceKM = distanceTraveled / 1000.0;
				double speedKM = distanceKM / totalHours;
				double distanceMI = distanceKM * MILES_PER_KILOMETER;
				double speedMI = distanceMI / totalHours;
				
				// display distance traveled and average speed
				dialogBuilder.setMessage(String.format(getResources().getString(R.string.results_format), distanceKM, distanceMI, speedKM, speedMI));
				dialogBuilder.setPositiveButton(R.string.button_ok, null);
				dialogBuilder.show();
			}
			else
			{
				tracking = true; // app is now tracking
				startTime = System.currentTimeMillis(); // get current time
				routeOverlay.reset(); // reset for a new route
				bearingFrameLayout.invalidate(); // clear the route
				previousLocation = null; // starting a new route
			}
		}
	};
	
	@Override
	protected void onStop()
	{
		super.onStop();
		
		// release the WakeLock
		wakeLock.release();
	}
	
	@Override
	protected boolean isRouteDisplayed()
	{
		return false;
	}
	
	/**
	 * Update location on map
	 * 
	 * @param location
	 */
	public void updateLocation(Location location)
	{
		if (location != null && gpsFix)
		{
			// add the given location to the route
			routeOverlay.addPoint(location);
			
			// if there is a previous location
			if (previousLocation != null)
			{
				// add to the total distanceTraveled
				distanceTraveled += location.distanceTo(previousLocation);
			}
			
			// get the latitude and longitude of the location
			Double latitude = location.getLatitude() * 1E6;
			Double longitude = location.getLongitude() * 1E6;
			
			// create GeoPoint representing the given Locations
			GeoPoint point = new GeoPoint(latitude.intValue(), longitude.intValue());
			
			// move the map to the current location
			mapController.animateTo(point);
			
			// update the compass bearing
			bearingFrameLayout.setBearing(location.getBearing());
			
			// redraw based on bearing
			bearingFrameLayout.invalidate(); 
		}
		
		previousLocation = location;
	}
	
	/**
	 * Create the activity's menu from a menu resource XML file
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.route_tracker_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.mapItem:
				mapView.setSatellite(false);
				return true;
			case R.id.satelliteItem:
				mapView.setSatellite(true);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Zoom in in and out of the map
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_MINUS:
				mapController.zoomOut();
				break;
			
			case KeyEvent.KEYCODE_EQUALS:
				mapController.zoomIn();
				break;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	/**
	 * Geting the location that was touched
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// when user lifts the finger
		if (event.getAction() == 1)
		{
			p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
			Toast.makeText(getBaseContext(), "Location: " + p.getLatitudeE6() / 1E6 + "," + p.getLongitudeE6() / 1E6, Toast.LENGTH_SHORT).show();
			// TODO: Reverse geocode the touched location on the map
		}
		
		return false;
	}
}
