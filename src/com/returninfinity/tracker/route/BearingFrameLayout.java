package com.returninfinity.tracker.route;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.view.Display;
import android.widget.FrameLayout;

import com.google.android.maps.MapView;

/**
 * Rotate MapView according to device's bearing (direction)
 * 
 */
public class BearingFrameLayout extends FrameLayout
{
	private int scale = 0; // amount to scale layout
	private final MapView mapView; // display Google maps
	private float bearing = 0f; // compass bearing
	
	public BearingFrameLayout(Context context, String apiKey)
	{
		super(context);
		
		mapView = new MapView(context, apiKey);
		mapView.setEnabled(true);
		mapView.setClickable(true);
		mapView.setSatellite(false);
		// mapView.setBuiltInZoomControls(true);
		mapView.displayZoomControls(true);
		mapView.setLayoutParams(getChildLayoutParams());
		addView(mapView);
	}
	
	/**
	 * 
	 * @return layout parameters for MapView
	 */
	public LayoutParams getChildLayoutParams()
	{
		Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
		Point p = new Point();
		display.getSize(p);
		int w = p.x;
		int h = p.y;
		scale = (int) Math.sqrt((w * w) + (h * h));
		return new LayoutParams(scale, scale);
	}
	
	public MapView getMapView()
	{
		return mapView;
	}
	
	public void setBearing(float bearing)
	{
		this.bearing = bearing;
	}
	
	/**
	 * Rotates the map according to bearing
	 */
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		if (bearing >= 0)
		{
			// get canvas dimensions
			int canvasWidth = canvas.getWidth();
			int canvasHeight = canvas.getHeight();
			
			// dimensions of the scaled canvas
			int width = scale;
			int height = scale;
			
			// center of scaled canvas
			int centerXScaled = width / 2;
			int centerYScaled = height / 2;
			
			// center of screen canvas
			int centerX = canvasWidth / 2;
			int centerY = canvasHeight / 2;
			
			// move center of scaled area to center of actual screen
			canvas.translate(-(centerXScaled - centerX), -(centerYScaled - centerY));
			
			// rotate around center of screen
			canvas.rotate(-bearing, centerXScaled, centerYScaled);
		}
		
		// draw child view of this layout
		super.dispatchDraw(canvas);
	}
}
