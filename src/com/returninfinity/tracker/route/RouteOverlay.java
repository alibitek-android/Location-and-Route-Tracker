package com.returninfinity.tracker.route;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/**
 * Draw route on MapView
 * 
 */
public class RouteOverlay extends Overlay
{
	private final List<Location> locations; 
	private final Paint pathPaint; 
	private final Paint positionPaint; 
	private final int POSITION_MARKER = 10; 
	
	public RouteOverlay()
	{
		pathPaint = new Paint();
		pathPaint.setAntiAlias(true);
		pathPaint.setColor(Color.RED);
		pathPaint.setStyle(Paint.Style.STROKE);
		pathPaint.setStrokeWidth(5);
		
		locations = new ArrayList<Location>();
		
		positionPaint = new Paint();
		positionPaint.setAntiAlias(true);
		positionPaint.setStyle(Paint.Style.FILL);
		positionPaint.setColor(Color.BLUE);
	}
	
	/**
	 * Add a new location to the list of locations
	 * 
	 * @param location
	 */
	public void addPoint(Location location)
	{
		locations.add(location);
	}
	
	/**
	 * Reset the Overlay for tracking a new route
	 */
	public void reset()
	{
		locations.clear();
	}
	
	/**
	 * Draw this Overlay on top of the given MapView
	 */
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow)
	{
		super.draw(canvas, mapView, shadow);
		
		Path newPath = new Path();
		Location previous = null;
		
		for (int i = 0; i < locations.size(); i++)
		{
			Location location = locations.get(i);
			
			Double newLatitude = location.getLatitude() * 1E6;
			Double newLongitude = location.getLongitude() * 1E6;
			GeoPoint newPoint = new GeoPoint(newLatitude.intValue(), newLongitude.intValue());
			
			// convert the GeoPoint to point on the screen
			Point newScreenPoint = new Point();
			mapView.getProjection().toPixels(newPoint, newScreenPoint);
			
			// if this is not the first location
			if (previous != null)
			{
				// get GeoPoint for the previous location
				Double oldLatitude = previous.getLatitude() * 1E6;
				Double oldLongitude = previous.getLongitude() * 1E6;
				GeoPoint oldPoint = new GeoPoint(oldLatitude.intValue(), oldLongitude.intValue());
				
				// convert the GeoPoint to point on the screen
				Point oldScreenPoint = new Point();
				mapView.getProjection().toPixels(oldPoint, oldScreenPoint);
				
				// add the new point to the path
				newPath.quadTo(oldScreenPoint.x, oldScreenPoint.y, (newScreenPoint.x + oldScreenPoint.x) / 2,
						(newScreenPoint.y + oldScreenPoint.y) / 2);
				
				// draw a black dot for current position
				if ((i % POSITION_MARKER) == 0)
					canvas.drawCircle(newScreenPoint.x, newScreenPoint.y, 10, positionPaint);
			}
			else
			{
				// move to the first location
				newPath.moveTo(newScreenPoint.x, newScreenPoint.y);
			}
			
			// store location
			previous = location;
		}
		
		// draw the path
		canvas.drawPath(newPath, pathPaint);
	}
}
