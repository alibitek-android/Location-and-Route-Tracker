package com.returninfinity.tracker.location;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.returninfinity.locationandroutetracker.R;

/**
 * Mobile device location
 * 
 */
public class LocationTracker extends MapActivity
{
	private MapView mapView;
	private MapController mapController;
	private GeoPoint p;
	private LocationManager locationManager;
	private LocationListener locationListener;	
	private TextView longitude, latitude;
	
	/**
	 * Animate Google Maps to the given location.
	 * 
	 * @param location
	 */
	private void centerMapToLocation(GeoPoint location)
	{
		mapController.animateTo(location);
		mapController.setZoom(13);
		
		longitude.setText("Longitude: " + String.valueOf(location.getLongitudeE6() / 1E6));
		latitude.setText("Latitude: " + String.valueOf(location.getLatitudeE6() / 1E6));
		
		// force the MapView to redraw
		mapView.invalidate();
	}
	
	private class MyMapOverlay extends Overlay
	{
		private GeoPoint pointToDraw;
		
		public void setPointToDraw(GeoPoint pointToDraw)
		{
			this.pointToDraw = pointToDraw;
		}
		
		@Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when)
		{
			super.draw(canvas, mapView, shadow);
			
			// translate the geographical location (represented by a GeoPoint object) to screen coordinates/pixels
			Point screenPts = new Point();
			mapView.getProjection().toPixels(pointToDraw, screenPts);
			
			// add the marker
			Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.redmarker);
			canvas.drawBitmap(bmp, screenPts.x, screenPts.y - 24, null);
			return true;
		}
	}
	
	private class MyLocationListener implements LocationListener
	{
		// Called when the location has changed
		public void onLocationChanged(Location location)
		{
			if (location != null)
			{
				Toast.makeText(getBaseContext(),
						"Location changed to: \n" + "Latitude: " + location.getLatitude() + "\n" + "Longitude: " + location.getLongitude(),
						Toast.LENGTH_LONG).show();
				
				p = new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));
				
				// navigate the map to the new location
				centerMapToLocation(p);
				
				// add a location marker to the map
				MyMapOverlay mapOverlay = new MyMapOverlay();
				mapOverlay.setPointToDraw(p);
				List<Overlay> listOverlays = mapView.getOverlays();
				listOverlays.clear();
				listOverlays.add(mapOverlay);
				
				mapView.invalidate();
				
				String address = convertPointToLocation(p);
				Toast.makeText(getBaseContext(), address, Toast.LENGTH_LONG).show();
				
				// Monitoring a location
				// (if the user is within a x-meters radius from the location specified,
				// the application will fire an intent to launch the web browser)
				PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0,
						new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://linuxgeek.ro")), 0);
				
				locationManager.addProximityAlert(45.660703, 25.613443, 100, -1, pendingIntent);
			}
		}
		
		// Called when the provider is disabled by the user
		public void onProviderDisabled(String provider)
		{
			Toast.makeText(getBaseContext(), provider + " disabled", Toast.LENGTH_SHORT).show();
		}
		
		// Called when the provided is enabled by the user
		public void onProviderEnabled(String provider)
		{
			Toast.makeText(getBaseContext(), provider + " enabled", Toast.LENGTH_SHORT).show();
		}
		
		// Called when the provider status changes
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			String statusString = "";
			switch (status)
			{
				case LocationProvider.AVAILABLE:
					statusString = "available";
					break;
				case LocationProvider.OUT_OF_SERVICE:
					statusString = "out of service";
					break;
				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					statusString = "temporarily unavailable";
					break;
			}
			
			Toast.makeText(getBaseContext(), provider + " " + statusString, Toast.LENGTH_SHORT).show();
		}
		
		/**
		 * Reverse geocoding
		 * From the given latitude and longitude of a location find out its address
		 * 
		 * @param point
		 * @return
		 */
		public String convertPointToLocation(GeoPoint point)
		{
			String address = "";
			Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
			try
			{
				List<Address> addresses = geoCoder.getFromLocation(point.getLatitudeE6() / 1E6, point.getLongitudeE6() / 1E6, 1);
				
				if (addresses.size() > 0)
				{
					for (int index = 0; index < addresses.get(0).getMaxAddressLineIndex(); index++)
						address += addresses.get(0).getAddressLine(index) + "\n";
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			
			return address;
		}
		
		/**
		 * Geocoding
		 * From the given location find out its GPS coordinates
		 * 
		 * @param location
		 * @return
		 */
		@SuppressWarnings("unused")
		public GeoPoint convertLocationToPoint(String location)
		{
			GeoPoint p = null;
			Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
			try
			{
				List<Address> addresses = geocoder.getFromLocationName(location, 5);
				
				if (addresses.size() > 0)
				{
					p = new GeoPoint((int) (addresses.get(0).getLatitude() / 1E6), (int) (addresses.get(0).getLongitude() / 1E6));
				}
			}
			catch (Exception e)
			{
			}
			return p;
		}
	}
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setSatellite(true);
		mapView.setTraffic(true);
		mapController = mapView.getController();
		mapController.setZoom(13);
		
		longitude = (TextView) findViewById(R.id.longitude);
		latitude = (TextView) findViewById(R.id.latitude);
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		
		// get the last known location
		p = new GeoPoint((int) (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude() * 1E6), (int) (locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude() * 1E6));
		centerMapToLocation(p);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
				
		// GPS location provider
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		
		// Cell ID and Wi-FI triangulation network provider (important for indoor use) to obtain location data
		// Determines location based on availability of cell tower and WiFi access points
		// DOESN'T WORK ON THE EMULATOR - ONLY WORKS ON A REAL DEVICE
		// lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60000, 200, locationListener);
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		
		// remove the location listener
		locationManager.removeUpdates(locationListener);
	}
	
	/**
	 * Used for Google accounting purposes and should return true if the map is going to display routing information
	 */
	@Override
	protected boolean isRouteDisplayed()
	{
		return false;
	}
	
	/**
	 * Zoom in and out of the map
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_MINUS:
				mapController.zoomOut();
				break;
			
			case KeyEvent.KEYCODE_EQUALS:
				mapController.zoomIn();
				break;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	/**
	 * Getting the location that was touched
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// when user lifts the finger
		if (event.getAction() == 1)
		{
			p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
			Toast.makeText(getBaseContext(), "Location: " + p.getLatitudeE6() / 1E6 + "," + p.getLongitudeE6() / 1E6, Toast.LENGTH_LONG).show();
			// TODO: Reverse geocode the touched location on the map
		}
		
		return false;
	}
}
