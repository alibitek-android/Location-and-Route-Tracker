package com.returninfinity.tracker.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Receiving SMS Messages (i.e. intercept incoming SMS messages)
 * 
 */
public class SmsReceiver extends BroadcastReceiver
{
	private LocationManager locationManager;
	private LocationListener locationListener;
	private String senderTel;
	
	@Override
	public void onReceive(Context context, Intent intent)
	{	
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		String str = "";
		
		if (bundle != null)
		{
			senderTel = "";
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			
			for (int i = 0; i < msgs.length; i++)
			{
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				if (i == 0)
				{
					senderTel = msgs[i].getOriginatingAddress();
				}
				
				str += "SMS from " + senderTel;
				str += " :";
				str += msgs[i].getMessageBody().toString();
				str += "\n";
			}
			
			Toast.makeText(context, str, Toast.LENGTH_LONG).show();
			Log.d("SmsReceiver", str);
			
			if (str.contains("COD"))
			{
				locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				
				locationListener = new MyLocationListener();
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000, 1000, locationListener);
				
				abortBroadcast();
				
				Toast.makeText(context, senderTel.substring(7), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private class MyLocationListener implements LocationListener
	{
		public void onLocationChanged(Location location)
		{
			if (location != null)
			{
				sendSMS(senderTel, "http://maps.google.com/maps?q=" + location.getLatitude() + "," + location.getLongitude());
			}
			
			locationManager.removeUpdates(locationListener);
		}
		
		public void onProviderDisabled(String provider)
		{
			
		}
		
		public void onProviderEnabled(String provider)
		{
			
		}
		
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			
		}
	}
	
	/**
	 * Send a SMS message to another device
	 * 
	 * @param phoneNo
	 * @param message
	 */
	private void sendSMS(String phoneNo, String message)
	{
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNo, null, message, null, null);
	}
}
